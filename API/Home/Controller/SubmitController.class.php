<?php
/**
 * Created by PhpStorm.
 * User: yzl
 * Date: 17/5/25
 * Time: 下午9:28
 */

namespace Home\Controller;
use Think\Controller;
class SubmitController extends Controller {
    public function index(){
        //$json = $GLOBALS['HTTP_RAW_POST_DATA'];
        $json = @file_get_contents('php://input', 'r');
        $jsonInfo = (array)json_decode($json);

        //获取发来的数据
        $user      = $jsonInfo['user'];
        $password  = $jsonInfo['password'];
        $student   = $jsonInfo['student'];
        $vector1   = $jsonInfo['vector'][0];
        $vector2   = $jsonInfo['vector'][1];
        $vector3   = $jsonInfo['vector'][2];
        $vector4   = $jsonInfo['vector'][3];
        $vector5   = $jsonInfo['vector'][4];
        $score     = (double)$vector1 + (double)$vector2 + (double)$vector3 + (double)$vector4 + (double)$vector5;

        $passw = M('rater')->field('passw',false)->where("rater_no = '$user'")->select();
        $passw = $passw[0]['passw'];

        if($password == $passw){

            //密码正确的时候

            //要插入的数据
            $gradedata = array(
                'rater_no'=>"$user",
                'competitor_no'=>"$student",
                'vector1'=>"$vector1",
                'vector2'=>"$vector2",
                'vector3'=>"$vector3",
                'vector4'=>"$vector4",
                'vector5'=>"$vector5",
                'total'=>"$score"
            );
            //插入数据
            M('grade')->add($gradedata);

            echo json_encode(array('status'=>0));

        }else{
            echo json_encode(array('status'=>1,'data'=>'用户名密码错误'));
        }
    }
}