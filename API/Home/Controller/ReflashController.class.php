<?php
/**
 * Created by PhpStorm.
 * User: yzl
 * Date: 17/5/25
 * Time: 下午9:05
 */

namespace Home\Controller;
use Think\Controller;
class ReflashController extends Controller {
    public function index(){
        //获取发送来数据
        //$json = $GLOBALS['HTTP_RAW_POST_DATA'];
        $json = @file_get_contents('php://input', 'r');
        $jsonInfo = (array)json_decode($json);

        //获取用户名和密码
        $user     = $jsonInfo['user'];
        $password = $jsonInfo['password'];

        //用户名和密码校验
        $passw = M('rater')->where("rater_no = '$user'")->field('passw',false)->select();
        $passw = $passw[0]['passw'];

        if($password == $passw){

            //用户名和密码正确的时候
            //取得该老师应该给谁打分了
            $competitor = M('grade')->where("rater_no = '$user'")->field('competitor_no',false)->order('competitor_no desc')->limit(0,1)->select();
            //dump($competitor);

            if(!empty($competitor))
            {
                //判断是不是最后一个（最后一个代表结束）
                if((int)$competitor[0]['competitor_no'] >= (int)C('LAST_NUMBER')){
                    echo json_encode(array('status'=>2,'data'=>'评分已经结束'));
                    exit;
                }else {
                    $competitor = (int)$competitor[0]['competitor_no'];

                    //获取该选手的数据
                    $option['competitor_no'] = array('gt', (int)$competitor);
                    $option['status'] = array('not in', '1');
                    $data = M('competitor')->where($option, 'and')->order('competitor_no asc')->limit(0, 1)->select();
                }
            }else{
                $option['status'] = array('not in','1');
                $data = M('competitor')->where($option)->order('competitor_no asc')->limit(0, 1)->select();
            }

            $competitor = $data[0]['competitor_no'];
            $xuhao = substr($competitor,2,2);
            $type = $data[0]['type'];
            $name = $data[0]['name'];
            $leader = $data[0]['leader'];

            $jsonarr = array('status'=>0,'id'=>"$competitor",'xuhao'=>$xuhao,'type'=>"$type",'name'=>"$name","leader"=>"$leader");
            echo json_encode($jsonarr);

        }else{
            echo json_encode(array('status'=>1,'data'=>'用户名密码错误'));
        }
    }
}