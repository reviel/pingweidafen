<?php
/**
 * Created by PhpStorm.
 * User: yzl
 * Date: 2017/5/30
 * Time: 10:59
 */

namespace Home\Controller;
use Think\Controller;
class CompetitorController extends Controller {
    public function index(){
        $json = @file_get_contents('php://input', 'r');
        $jsonInfo = (array)json_decode($json);

        $competitor = $jsonInfo['competitor'];

        //获取数据
        $data = M('grade')->where("competitor_no = '$competitor'")->field('rater_no,total',false)->order('rater_no asc')->select();
        $teacher = array();
        $scores  = array();
        foreach ($data as $key => $value){
            array_push($teacher,$value['rater_no']);
        }
        foreach ($data as $key => $value){
            array_push($scores,$value['total']);
        }

        //统计分数
        $data1 = M('grade')->where("competitor_no = '$competitor'")->field('total',false)->order('total asc')->select();

        $ave = array();
        foreach ($data1 as $key => $value){
            if($key!=0){
                array_push($ave,$value['total']);
            }
        }
        array_pop($ave);

        $sum = 0;
        $geshu = 0;
        foreach ($ave as $key => $value){
            $geshu = $key+1;
            $sum += (double)$value;
        }
        $average = (double)$sum/$geshu;


        echo json_encode(array('competitor'=>$competitor,'teacher'=>$teacher,'score'=>$scores,'ave'=>$average));

    }
}